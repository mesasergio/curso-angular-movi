import { Component, OnInit, Input } from '@angular/core';
import swal from 'sweetalert2';
import { AuthFirebaseService } from '../../providers/auth/auth-firebase.service';


@Component({
  selector: "app-componente-perfil",
  templateUrl: "./componente-perfil.component.html",
  styleUrls: ["./componente-perfil.component.scss"]
})
export class ComponentePerfilComponent implements OnInit {
  @Input() nombre: any = "";
  @Input() correo: any = "";
  @Input() foto: any = "";

  constructor(private _auth: AuthFirebaseService) {}

  ngOnInit() {

    this.nombre = this.nombre ? this.nombre : "Sin nombre";
    this.correo = this.correo ? this.correo : "Sin correo";
    this.foto = this.foto
      ? this.foto
      : "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";

  }

  salir() {
    swal
      .fire({
        title: "Confirmación",
        text: "¿Está seguro de salir?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Salir"
      })
      .then(result => {
        if (result.value) {
          this._auth.signOut();
        }
      });
  }
}
