import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentePerfilComponent } from './componente-perfil.component';

describe('ComponentePerfilComponent', () => {
  let component: ComponentePerfilComponent;
  let fixture: ComponentFixture<ComponentePerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentePerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentePerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
