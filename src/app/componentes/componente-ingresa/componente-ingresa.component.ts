import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { AuthFirebaseService } from '../../providers/auth/auth-firebase.service';

@Component({
  selector: "app-componente-ingresa",
  templateUrl: "./componente-ingresa.component.html",
  styleUrls: ["./componente-ingresa.component.scss"]
})

export class ComponenteIngresaComponent {

  loginForm = this.fb.group({
    email: [null, [Validators.required, Validators.email] ],
    pass: [null, Validators.required]
  });

  formSubmitted:boolean = false;

  constructor(
    private fb: FormBuilder,
    private _auth: AuthFirebaseService
    ) {}

  onSubmit() {

    // Controla que el formulario se halla enviado
    this.formSubmitted = true;

    // Valida si el formulario reactivo tiene error
    if(!this.loginForm.valid) return;

    this._auth.signInWithEmail(this.loginForm.value.email,this.loginForm.value.pass).then((user:firebase.auth.UserCredential)=>{
      swal.fire({
        type: 'success',
        title: 'Bienvido',
        text: `Bienvenido ${user.user.email} nos encanta que volvieras`
      })
    }).catch((error:firebase.FirebaseError)=>{
      swal.fire({
        type: 'error',
        title: 'Error',
        text: `Ups, ocurrio un error: ${error.message}`
      })
    })
  }

}
