import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthFirebaseService } from 'src/app/providers/auth/auth-firebase.service';
import swal from "sweetalert2";


@Component({
  selector: "app-componente-registro",
  templateUrl: "./componente-registro.component.html",
  styleUrls: ["./componente-registro.component.scss"]
})
export class ComponenteRegistroComponent {

  regForm = this.fb.group({
    nombre: [null, Validators.required],
    apellido: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    pass: [null, [Validators.required, Validators.minLength(6)]],
    telefono: [null, [Validators.required]],
  });

  formSubmitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private _auth: AuthFirebaseService
  ) {}

  onSubmit() {
    // Controla que el formulario se halla enviado
    this.formSubmitted = true;

    // Valida si el formulario reactivo tiene error
    if (!this.regForm.valid) return;

    this._auth.signUpWithEmail(this.regForm.value.email,this.regForm.value.pass).then((user:firebase.auth.UserCredential)=>{
        swal.fire({
          type: 'success',
          title: 'Bienvenido',
          text: `${user.user.email} nos encanta tenerte aqui`
        });
    }).catch(((error:firebase.FirebaseError)=>{
        swal.fire({
          type: 'error',
          title: 'Error',
          text: `Ups, ocurrio un error: ${error.message}`
        })
    }));

  }
}
