import { Component, OnInit } from '@angular/core';
import { AuthFirebaseService } from '../../providers/auth/auth-firebase.service';

@Component({
  selector: 'app-pagina-ingreso',
  templateUrl: './pagina-ingreso.component.html',
  styleUrls: ['./pagina-ingreso.component.scss']
})
export class PaginaIngresoComponent implements OnInit {

  autenticado:boolean = false;
  user:firebase.User;
  nombre:any;

  constructor(
    private _auth:AuthFirebaseService
  ) { }

  ngOnInit() {
    this._auth.getCurrentUser.subscribe((user:firebase.User)=>{
      this.autenticado = (user != null);
      this.user = user;
      if(user){
        this.nombre = user.email;
      }
    })
  }

}
