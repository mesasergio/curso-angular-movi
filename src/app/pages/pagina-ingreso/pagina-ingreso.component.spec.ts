import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaIngresoComponent } from './pagina-ingreso.component';

describe('PaginaIngresoComponent', () => {
  let component: PaginaIngresoComponent;
  let fixture: ComponentFixture<PaginaIngresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaIngresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
