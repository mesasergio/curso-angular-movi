import { TestBed } from '@angular/core/testing';

import { ModelFirebaseService } from './model-firebase.service';

describe('ModelFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModelFirebaseService = TestBed.get(ModelFirebaseService);
    expect(service).toBeTruthy();
  });
});
