import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as firebase from "firebase/app";
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: "root"
})
export class AuthFirebaseService {

  user: Observable<firebase.User | null>;


  constructor(private auth: AngularFireAuth) {
    this.user = this.auth.authState;
  }

  // Saber si el usuario esta autenticado
  get authenticated(): boolean {
    return this.user != null;
  }

  // Retorna el usuario autenticado
  get getCurrentUser(): Observable<firebase.User | null> {
    return this.user;
  }

  // Registro por email
  signUpWithEmail(email, password): Promise<firebase.auth.UserCredential> {
    return this.auth.auth.createUserWithEmailAndPassword(email, password);
  }

  // Autenticar por email
  signInWithEmail(email, password): Promise<firebase.auth.UserCredential> {
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }

  // Cerrar sesión
  signOut():Promise<void>{
    return this.auth.auth.signOut();
  }

  // Recuperar contraseña
  resetPassword(email):Promise<void>{
    return this.auth.auth.sendPasswordResetEmail(email);
  }

  // Verificar Correo
  verifyEmail():Promise<void>{
    return firebase.auth().currentUser.sendEmailVerification();
  }

}
