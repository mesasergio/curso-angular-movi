**APUNTES DE CLASE**

*Instalar Angular y Angular CLI*

`npm install -g @angular/cli
`

*Crear proyecto Angular*

`ng new <nombre-app>
`

*Generar un componente Angular*

`ng g componente <nombre-componente>
`

*Instalar dependencia*

`npm i <nombre-dependencia>@<version> -S
`

*Lanzar servidor livereload*

`ng serve --open
`

*Instalar Material Angular con Schematics*

`ng add @angular/material
`

*Instalar Angular CDK con Schaematics*

`ng add @angular/cdk
`

*Crear componente form address con Schematics*

`ng g @angular/material:address-form <nombre-componente>
`

*Crear componente dashboard con Schematics*

`ng g @angular/material:dashboard <nombre-componente>
`

*Crear componente barra de navegacion con Schematics*

`ng g @angular/material:nav <nombre-componente>
`

*Crear componente tabla con Schematics*

`ng g @angular/material:table <nombre-componente>
`

*Crear componente Arbol con Schematics*

`ng g @angular/material:tree <nombre-componente>
`

*Crear componente Drag and Drop con Schematics*

`ng g @angular/cdk:drag-drop <nombre-componente>
`


**TIPOS DE VARIABLES TYPESCRIPT**

*Booleano*

> El tipo de datos más básico es el simple valor verdadero / falso, al que JavaScript y TypeScript llaman un booleanvalor. 

`let varBool: boolean = false;`


*Número*

> Como en JavaScript, todos los números en TypeScript son valores de punto flotante. Estos números de punto flotante obtienen el tipo number. Además de los literales hexadecimales y decimales, TypeScript también admite literales binarios y octales introducidos en ECMAScript 2015. 

```
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
```

*String*

> Otra parte fundamental de la creación de programas en JavaScript para páginas web y servidores es trabajar con datos textuales. Como en otros lenguajes, usamos el tipo string para referirnos a estos tipos de datos textuales. Al igual que JavaScript, TypeScript también usa comillas dobles ( ") o comillas simples ( ') para rodear los datos de cadena. 

```
let color: string = "azul";
color = 'red';
```


> También puede usar cadenas de plantillas , que pueden abarcar varias líneas y tener expresiones incrustadas. Estas cadenas están rodeadas por el carácter backtick / backquote ( `), y las expresiones incrustadas son de la forma ${ expr }. 

```
let fullName: string = `Sergio Mesa`;
let age: number = 34;
let sentence: string = `Hola, mi nombre es ${ fullName }.

yo voy a cumplir ${ age + 1 } años el proximo mes.`;
```



> Esto es equivalente a declarar sentenceasí: 

```
let sentence: string = "Hola, mi nombre es " + fullName + ".\n\n" +
    "yo voy a cumplir " + (age + 1) + " años el proximo mes.";
```


*Arreglos*

> TypeScript, como JavaScript, le permite trabajar con matrices de valores. Los tipos de matriz se pueden escribir de una de dos maneras. En la primera, utiliza el tipo de los elementos seguidos por [] para denotar una matriz de ese tipo de elemento: 

```
let list: number[] = [1, 2, 3];
La segunda forma utiliza un genérico tipo de matriz, Array<elemType>:

let list: Array<number> = [1, 2, 3];
```

*Tuple*


> Los tipos de tuplas le permiten expresar una matriz donde se conoce el tipo de un número fijo de elementos, pero no es necesario que sean iguales. Por ejemplo, puede querer representar un valor como un par de a stringy a number: 

```
// Declaracion de tipo tuple
let x: [string, number];

// Inicialización
x = ["hello", 10]; // OK

// Inicialización incorrecta
x = [10, "hello"]; // Error
```


> Al acceder a un elemento con un índice conocido, se recupera el tipo correcto: 

```
console.log(x[0].substr(1)); // OK
console.log(x[1].substr(1)); // Error, 'number' does not have 'substr'
```


> Al acceder a un elemento fuera del conjunto de índices conocidos, en su lugar se utiliza un tipo de unión:

```
x[3] = "Mundo"; // OK, 'string' puede ser asignado a un string | number

console.log(x[5].toString()); // OK, 'string' y 'number' ambos tienen 'toString'

x[6] = true; // Error, 'boolean' no es un 'string | number'
```


> Los tipos de unión son un tema avanzado que cubriremos en un capítulo posterior. 


*Enumeradores*


> Una adición útil al conjunto estándar de tipos de datos de JavaScript es el enum. Como en lenguajes como C #, una enumeración es una forma de dar nombres más amigables a conjuntos de valores numéricos. 

```
enum Color {Red, Green, Blue}
let c: Color = Color.Green;
```



> Por defecto, las enumeraciones comienzan numerando a sus miembros comenzando en 0. Puede cambiar esto configurando manualmente el valor de uno de sus miembros. Por ejemplo, podemos comenzar el ejemplo anterior en 1lugar de 0: 

```
enum Color {Red = 1, Green, Blue}
let c: Color = Color.Green;
```

> O bien, incluso establecer manualmente todos los valores en la enumeración: 

```
enum Color {Red = 1, Green = 2, Blue = 4}
let c: Color = Color.Green;
```

> Una característica útil de las enumeraciones es que también puede pasar de un valor numérico al nombre de ese valor en la enumeración. Por ejemplo, si tuviéramos el valor 2pero no estuviéramos seguros de lo que se asignó en la Colorenumeración anterior, podríamos buscar el nombre correspondiente: 

```
enum Color {Red = 1, Green, Blue}
let colorName: string = Color[2];

console.log(colorName); // Muestra 'Green' 
```








